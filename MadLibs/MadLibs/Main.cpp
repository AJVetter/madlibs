
// MadLibs
// AJ Vetter, George Mitze, Mason Brull

#include <iostream>
#include <conio.h>
#include <string>
#include <fstream>

using namespace std;

int main()
{
	string prompts[] = { "Enter an adjective (describing word): ",
						"Enter a sport: ",
						"Enter a city: ",
						"Enter a person: ",
						"Enter an action verb (past tense): ",
						"Enter a vehicle: ",
						"Enter a place: ",
						"Enter a noun: ",
						"Enter an adjective (describing word): ",
						"Enter a food:",
						"Enter a liquid: ",
						"Enter an adjective (describing word): " };

	const int size = 12;
	string words[size];

	for (int i = 0; i < size; i++)
	{
		cout << prompts[i];
		getline(cin, words[i]);
		cout << endl;
	}

	const int lineCount = 6;
	string lines[lineCount]{ "One day my " + words[0] + " friend and I decided to go to the " + words[1] + " game in " + words[2] + ".\n",
					"We really wanted to see " + words[3] + " play.\n",
					"So we " + words[4] + " in the " + words[5] + " and headed down to the " + words[6] + " and bought some " + words[7] + ".\n",
					"We watched the game and it was " + words[8] + ".\n",
					"We ate some " + words[9] + " and drank some " + words[10] + ".\n",
					"We had a " + words[11] + " time and can't wait to go again.\n" };

	for (int i = 0; i < lineCount; i++) 
		cout << lines[i];

	cout << endl << "would you like to output to file? (y/n): ";
	string input;
	getline(cin, input);

	if (input == "y")
	{
		ofstream ofs("madlibs.txt");

		for (int i = 0; i < lineCount; i++)
			ofs << lines[i];

		ofs.close();

		cout << "Mad Lib saved to madlib.txt.";
	}

	(void)_getch();
	return 0;
}
